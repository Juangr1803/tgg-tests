/**
 * 
 * 
 * Implements a custom type into a interface, and implements the interface into a Class.
 * 
 * Construct the class and add the variables into the constructor
 */


 type {}
 interface {}

 class Test {
     constructor(){}
 }

 new Test()


// Fix the following error and add the right types:

function average(a, b) {
    return a + b / 2;
  }
  
  console.log(average(2, 1));
  
  /**
   *
   * IMPLEMENTS THE removeProperty function, wich takes an object and property name and does the following:
   *  *
   * 1. Convert into a arrow function
   * 2. If the object obj has a property prop, the function removes the property from the object and returns true; in all other cases it returns false   * 
   * 3. Remember to add the right types.
   * 
   */
  function removeProperty(obj, prop) {
    return null;
  }
  
  
  /**
   *
   *
   * Start an express app.
   *
   * Define three different endpoints:
   *
   * 1. One for get method
   * 2. One for post method
   * 3. One for delete method.
   *
   * for the operations, use array functions
   *
   * Expose the data via Express
   *  Implement a middleware
   *
   * Use the data in data/dummy.js
   */
  
 import express from 'express'
  
  /**
   *
   *
   * Read and print the ENV VARIABLES from .env
   *
   *
   */
  