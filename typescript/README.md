**Torneos.GG Test: Typescript**

This test has been made to validate the knowledge of a job applicant. The test duration should not be above 30 minutes.

**What you need:**

1. Internet connection
2. A code editor
3. Git
4. Share your screen

**The goal:**

1. Complete the index.ts instructions
