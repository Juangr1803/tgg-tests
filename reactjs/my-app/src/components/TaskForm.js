// React
import React, { useState } from "react";
// Styles
import "../assets/styles/components/TaskForm.css";

const TaskForm = ({ addNewTask }) => {
  const initialState = {
    name: "",
    task: "",
  };

  const [newTask, setNewTask] = useState(initialState);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewTask({
      ...newTask,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (newTask.name && newTask.task) {
      addNewTask(newTask);
      setNewTask(initialState);
    }
  };

  return (
    <form className="task-new__form" onSubmit={handleSubmit}>
      <label className="task-new__form--item" htmlFor="name">
        <span>Name</span>
        <input
          name="name"
          id="name"
          placeholder="Insert your Name"
          onChange={handleChange}
          value={newTask.name}
        />
      </label>
      <label className="task-new__form--item" htmlFor="task">
        <span>Task</span>
        <input
          name="task"
          id="task"
          placeholder="Insert your Task"
          onChange={handleChange}
          value={newTask.task}
        />
      </label>
      <button className="btn btn-primary" type="submit">
        Create Task
      </button>
    </form>
  );
};

export default TaskForm;
