import React from "react";
import book from "../assets/static/book.png";
import "../assets/styles/components/Header.css";

const Header = ({ task }) => {
  return (
    <header className="header">
      <div className="header-container">
        <h1 className="header__title">Things To Do {task.length} </h1>
        <div className="header__title-image">
          <img className="header__title--image" src={book} alt="book" />
          {task.length}
        </div>
      </div>
    </header>
  );
};

export default Header;
