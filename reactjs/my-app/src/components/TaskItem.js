import React from "react";

import "../assets/styles/pages/Task.css";

const TaskItem = ({ task, i }) => {
  return (
    <div className="tasks-body" key={task.name}>
      <p>{i + 1}</p>
      <p>{task.name}</p>
      <p>{task.task}</p>
    </div>
  );
};

export default TaskItem;
