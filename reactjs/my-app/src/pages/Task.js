import React from "react";

// Components
import TaskForm from "../components/TaskForm";
import TaskItem from "../components/TaskItem";

// Styles
import "../assets/styles/pages/Task.css";
import "../assets/styles/pages/TaskNew.css";

const Task = ({ task, setTask }) => {
  const addNewTask = (newTask) => {
    console.log(newTask);
    setTask([...task, newTask]);
  };

  return (
    <div className="task">
      <div className="task-container">
        <div className="task-new">
          <div className="task-new-container">
            <TaskForm addNewTask={addNewTask} />
          </div>
        </div>
        <div className="tasks">
          <div className="tasks-container">
            <div className="tasks-head">
              <p>#</p>
              <p>Name</p>
              <p>Task</p>
            </div>
            <div>
              {task.map((item, i) => (
                <TaskItem className="tasks-body" task={item} key={i} i={i} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Task;
