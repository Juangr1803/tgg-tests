// React
import React, { useState, useEffect } from "react";
// Components
import Header from "./components/Header";
import Task from "./pages/Task";
// Styles
import "./App.css";

const App = () => {
  const [task, setTask] = useState([]);

  useEffect(() => {
    let data = localStorage.getItem("task");
    console.log(JSON.parse(data));
    if (data != null) {
      setTask(JSON.parse(data));
    } else {
      setTask([
        {
          name: "Juan",
          task: "Hacer deploy de la aplicacion de React",
        },
      ]);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("task", JSON.stringify(task));
  }, [task]);

  return (
    <div>
      <Header task={task} />
      <Task task={task} setTask={setTask} />
    </div>
  );
};

export default App;
