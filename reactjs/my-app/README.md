**Torneos.GG Test: ReacTJS**

This test has been made to validate the knowledge of a job applicant. The test duration should not be above 30 minutes.

**What you need:**

1. Internet connection
2. A code editor
3. Git
4. Share your screen

**The goal:** 
1. Replicate the following app (Todo app), with the use of stateless components, hooks, localStorage.
2. The most important part of this test is the logic, the variables name, the app flow. Maybe you will not have enough time to design the entire app with CSS. 
3. You can add redux, mobx or graphql if you think there needed. 
4. You can, also, use Next.JS, Gasby or ReactNative.


![to-do app](https://miro.medium.com/max/873/1*hPjPhc2gYkTplzF_DH-g5Q.png)
