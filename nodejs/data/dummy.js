const data = [
    {
      "name": "Luke Skywalker",
      "birthYear": "19BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "blond",
      "height": 172,
      "mass": 77,
      "skinColor": "fair",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "C-3PO",
      "birthYear": "112BBY",
      "eyeColor": "yellow",
      "gender": "n/a",
      "hairColor": "n/a",
      "height": 167,
      "mass": 75,
      "skinColor": "gold",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "R2-D2",
      "birthYear": "33BBY",
      "eyeColor": "red",
      "gender": "n/a",
      "hairColor": "n/a",
      "height": 96,
      "mass": 32,
      "skinColor": "white, blue",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Darth Vader",
      "birthYear": "41.9BBY",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 202,
      "mass": 136,
      "skinColor": "white",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Leia Organa",
      "birthYear": "19BBY",
      "eyeColor": "brown",
      "gender": "female",
      "hairColor": "brown",
      "height": 150,
      "mass": 49,
      "skinColor": "light",
      "homeworld": {
        "name": "Alderaan",
        "diameter": 12500,
        "rotationPeriod": 24,
        "orbitalPeriod": 364,
        "gravity": "1 standard",
        "population": 2000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grasslands",
          "mountains"
        ],
        "surfaceWater": 40,
        "residentConnection": {
          "residents": [
            {
              "name": "Leia Organa",
              "birthYear": "19BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 150,
              "mass": 49,
              "skinColor": "light"
            },
            {
              "name": "Bail Prestor Organa",
              "birthYear": "67BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 191,
              "mass": null,
              "skinColor": "tan"
            },
            {
              "name": "Raymus Antilles",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 188,
              "mass": 79,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Owen Lars",
      "birthYear": "52BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown, grey",
      "height": 178,
      "mass": 120,
      "skinColor": "light",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Beru Whitesun lars",
      "birthYear": "47BBY",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "brown",
      "height": 165,
      "mass": 75,
      "skinColor": "light",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "R5-D4",
      "birthYear": "unknown",
      "eyeColor": "red",
      "gender": "n/a",
      "hairColor": "n/a",
      "height": 97,
      "mass": 32,
      "skinColor": "white, red",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Biggs Darklighter",
      "birthYear": "24BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 183,
      "mass": 84,
      "skinColor": "light",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Obi-Wan Kenobi",
      "birthYear": "57BBY",
      "eyeColor": "blue-gray",
      "gender": "male",
      "hairColor": "auburn, white",
      "height": 182,
      "mass": 77,
      "skinColor": "fair",
      "homeworld": {
        "name": "Stewjon",
        "diameter": 0,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "1 standard",
        "population": null,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grass"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Obi-Wan Kenobi",
              "birthYear": "57BBY",
              "eyeColor": "blue-gray",
              "gender": "male",
              "hairColor": "auburn, white",
              "height": 182,
              "mass": 77,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Anakin Skywalker",
      "birthYear": "41.9BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "blond",
      "height": 188,
      "mass": 84,
      "skinColor": "fair",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Wilhuff Tarkin",
      "birthYear": "64BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "auburn, grey",
      "height": 180,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Eriadu",
        "diameter": 13490,
        "rotationPeriod": 24,
        "orbitalPeriod": 360,
        "gravity": "1 standard",
        "population": 22000000000,
        "climates": [
          "polluted"
        ],
        "terrains": [
          "cityscape"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Wilhuff Tarkin",
              "birthYear": "64BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "auburn, grey",
              "height": 180,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Chewbacca",
      "birthYear": "200BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 228,
      "mass": 112,
      "skinColor": "unknown",
      "homeworld": {
        "name": "Kashyyyk",
        "diameter": 12765,
        "rotationPeriod": 26,
        "orbitalPeriod": 381,
        "gravity": "1 standard",
        "population": 45000000,
        "climates": [
          "tropical"
        ],
        "terrains": [
          "jungle",
          "forests",
          "lakes",
          "rivers"
        ],
        "surfaceWater": 60,
        "residentConnection": {
          "residents": [
            {
              "name": "Chewbacca",
              "birthYear": "200BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 228,
              "mass": 112,
              "skinColor": "unknown"
            },
            {
              "name": "Tarfful",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 234,
              "mass": 136,
              "skinColor": "brown"
            }
          ]
        }
      }
    },
    {
      "name": "Han Solo",
      "birthYear": "29BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "brown",
      "height": 180,
      "mass": 80,
      "skinColor": "fair",
      "homeworld": {
        "name": "Corellia",
        "diameter": 11000,
        "rotationPeriod": 25,
        "orbitalPeriod": 329,
        "gravity": "1 standard",
        "population": 3000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "plains",
          "urban",
          "hills",
          "forests"
        ],
        "surfaceWater": 70,
        "residentConnection": {
          "residents": [
            {
              "name": "Han Solo",
              "birthYear": "29BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 180,
              "mass": 80,
              "skinColor": "fair"
            },
            {
              "name": "Wedge Antilles",
              "birthYear": "21BBY",
              "eyeColor": "hazel",
              "gender": "male",
              "hairColor": "brown",
              "height": 170,
              "mass": 77,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Greedo",
      "birthYear": "44BBY",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "n/a",
      "height": 173,
      "mass": 74,
      "skinColor": "green",
      "homeworld": {
        "name": "Rodia",
        "diameter": 7549,
        "rotationPeriod": 29,
        "orbitalPeriod": 305,
        "gravity": "1 standard",
        "population": 1300000000,
        "climates": [
          "hot"
        ],
        "terrains": [
          "jungles",
          "oceans",
          "urban",
          "swamps"
        ],
        "surfaceWater": 60,
        "residentConnection": {
          "residents": [
            {
              "name": "Greedo",
              "birthYear": "44BBY",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "n/a",
              "height": 173,
              "mass": 74,
              "skinColor": "green"
            }
          ]
        }
      }
    },
    {
      "name": "Jabba Desilijic Tiure",
      "birthYear": "600BBY",
      "eyeColor": "orange",
      "gender": "hermaphrodite",
      "hairColor": "n/a",
      "height": 175,
      "mass": 1358,
      "skinColor": "green-tan, brown",
      "homeworld": {
        "name": "Nal Hutta",
        "diameter": 12150,
        "rotationPeriod": 87,
        "orbitalPeriod": 413,
        "gravity": "1 standard",
        "population": 7000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "urban",
          "oceans",
          "swamps",
          "bogs"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Jabba Desilijic Tiure",
              "birthYear": "600BBY",
              "eyeColor": "orange",
              "gender": "hermaphrodite",
              "hairColor": "n/a",
              "height": 175,
              "mass": 1358,
              "skinColor": "green-tan, brown"
            }
          ]
        }
      }
    },
    {
      "name": "Wedge Antilles",
      "birthYear": "21BBY",
      "eyeColor": "hazel",
      "gender": "male",
      "hairColor": "brown",
      "height": 170,
      "mass": 77,
      "skinColor": "fair",
      "homeworld": {
        "name": "Corellia",
        "diameter": 11000,
        "rotationPeriod": 25,
        "orbitalPeriod": 329,
        "gravity": "1 standard",
        "population": 3000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "plains",
          "urban",
          "hills",
          "forests"
        ],
        "surfaceWater": 70,
        "residentConnection": {
          "residents": [
            {
              "name": "Han Solo",
              "birthYear": "29BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 180,
              "mass": 80,
              "skinColor": "fair"
            },
            {
              "name": "Wedge Antilles",
              "birthYear": "21BBY",
              "eyeColor": "hazel",
              "gender": "male",
              "hairColor": "brown",
              "height": 170,
              "mass": 77,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Jek Tono Porkins",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 180,
      "mass": 110,
      "skinColor": "fair",
      "homeworld": {
        "name": "Bestine IV",
        "diameter": 6400,
        "rotationPeriod": 26,
        "orbitalPeriod": 680,
        "gravity": "unknown",
        "population": 62000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "rocky islands",
          "oceans"
        ],
        "surfaceWater": 98,
        "residentConnection": {
          "residents": [
            {
              "name": "Jek Tono Porkins",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 180,
              "mass": 110,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Yoda",
      "birthYear": "896BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "white",
      "height": 66,
      "mass": 17,
      "skinColor": "green",
      "homeworld": {
        "name": "unknown",
        "diameter": 0,
        "rotationPeriod": 0,
        "orbitalPeriod": 0,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yoda",
              "birthYear": "896BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 66,
              "mass": 17,
              "skinColor": "green"
            },
            {
              "name": "IG-88",
              "birthYear": "15BBY",
              "eyeColor": "red",
              "gender": "none",
              "hairColor": "none",
              "height": 200,
              "mass": 140,
              "skinColor": "metal"
            },
            {
              "name": "Arvel Crynyd",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": null,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Qui-Gon Jinn",
              "birthYear": "92BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 193,
              "mass": 89,
              "skinColor": "fair"
            },
            {
              "name": "R4-P17",
              "birthYear": "unknown",
              "eyeColor": "red, blue",
              "gender": "female",
              "hairColor": "none",
              "height": 96,
              "mass": null,
              "skinColor": "silver, red"
            }
          ]
        }
      }
    },
    {
      "name": "Palpatine",
      "birthYear": "82BBY",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "grey",
      "height": 170,
      "mass": 75,
      "skinColor": "pale",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Boba Fett",
      "birthYear": "31.5BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 183,
      "mass": 78.2,
      "skinColor": "fair",
      "homeworld": {
        "name": "Kamino",
        "diameter": 19720,
        "rotationPeriod": 27,
        "orbitalPeriod": 463,
        "gravity": "1 standard",
        "population": 1000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "ocean"
        ],
        "surfaceWater": 100,
        "residentConnection": {
          "residents": [
            {
              "name": "Boba Fett",
              "birthYear": "31.5BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 78.2,
              "skinColor": "fair"
            },
            {
              "name": "Lama Su",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 229,
              "mass": 88,
              "skinColor": "grey"
            },
            {
              "name": "Taun We",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "female",
              "hairColor": "none",
              "height": 213,
              "mass": null,
              "skinColor": "grey"
            }
          ]
        }
      }
    },
    {
      "name": "IG-88",
      "birthYear": "15BBY",
      "eyeColor": "red",
      "gender": "none",
      "hairColor": "none",
      "height": 200,
      "mass": 140,
      "skinColor": "metal",
      "homeworld": {
        "name": "unknown",
        "diameter": 0,
        "rotationPeriod": 0,
        "orbitalPeriod": 0,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yoda",
              "birthYear": "896BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 66,
              "mass": 17,
              "skinColor": "green"
            },
            {
              "name": "IG-88",
              "birthYear": "15BBY",
              "eyeColor": "red",
              "gender": "none",
              "hairColor": "none",
              "height": 200,
              "mass": 140,
              "skinColor": "metal"
            },
            {
              "name": "Arvel Crynyd",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": null,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Qui-Gon Jinn",
              "birthYear": "92BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 193,
              "mass": 89,
              "skinColor": "fair"
            },
            {
              "name": "R4-P17",
              "birthYear": "unknown",
              "eyeColor": "red, blue",
              "gender": "female",
              "hairColor": "none",
              "height": 96,
              "mass": null,
              "skinColor": "silver, red"
            }
          ]
        }
      }
    },
    {
      "name": "Bossk",
      "birthYear": "53BBY",
      "eyeColor": "red",
      "gender": "male",
      "hairColor": "none",
      "height": 190,
      "mass": 113,
      "skinColor": "green",
      "homeworld": {
        "name": "Trandosha",
        "diameter": 0,
        "rotationPeriod": 25,
        "orbitalPeriod": 371,
        "gravity": "0.62 standard",
        "population": 42000000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "mountains",
          "seas",
          "grasslands",
          "deserts"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Bossk",
              "birthYear": "53BBY",
              "eyeColor": "red",
              "gender": "male",
              "hairColor": "none",
              "height": 190,
              "mass": 113,
              "skinColor": "green"
            }
          ]
        }
      }
    },
    {
      "name": "Lando Calrissian",
      "birthYear": "31BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 177,
      "mass": 79,
      "skinColor": "dark",
      "homeworld": {
        "name": "Socorro",
        "diameter": 0,
        "rotationPeriod": 20,
        "orbitalPeriod": 326,
        "gravity": "1 standard",
        "population": 300000000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "deserts",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Lando Calrissian",
              "birthYear": "31BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 177,
              "mass": 79,
              "skinColor": "dark"
            }
          ]
        }
      }
    },
    {
      "name": "Lobot",
      "birthYear": "37BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "none",
      "height": 175,
      "mass": 79,
      "skinColor": "light",
      "homeworld": {
        "name": "Bespin",
        "diameter": 118000,
        "rotationPeriod": 12,
        "orbitalPeriod": 5110,
        "gravity": "1.5 (surface), 1 standard (Cloud City)",
        "population": 6000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "gas giant"
        ],
        "surfaceWater": 0,
        "residentConnection": {
          "residents": [
            {
              "name": "Lobot",
              "birthYear": "37BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "none",
              "height": 175,
              "mass": 79,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Ackbar",
      "birthYear": "41BBY",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 180,
      "mass": 83,
      "skinColor": "brown mottle",
      "homeworld": {
        "name": "Mon Cala",
        "diameter": 11030,
        "rotationPeriod": 21,
        "orbitalPeriod": 398,
        "gravity": "1",
        "population": 27000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "oceans",
          "reefs",
          "islands"
        ],
        "surfaceWater": 100,
        "residentConnection": {
          "residents": [
            {
              "name": "Ackbar",
              "birthYear": "41BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 180,
              "mass": 83,
              "skinColor": "brown mottle"
            }
          ]
        }
      }
    },
    {
      "name": "Mon Mothma",
      "birthYear": "48BBY",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "auburn",
      "height": 150,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Chandrila",
        "diameter": 13500,
        "rotationPeriod": 20,
        "orbitalPeriod": 368,
        "gravity": "1",
        "population": 1200000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "plains",
          "forests"
        ],
        "surfaceWater": 40,
        "residentConnection": {
          "residents": [
            {
              "name": "Mon Mothma",
              "birthYear": "48BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "auburn",
              "height": 150,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Arvel Crynyd",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "brown",
      "height": null,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "unknown",
        "diameter": 0,
        "rotationPeriod": 0,
        "orbitalPeriod": 0,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yoda",
              "birthYear": "896BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 66,
              "mass": 17,
              "skinColor": "green"
            },
            {
              "name": "IG-88",
              "birthYear": "15BBY",
              "eyeColor": "red",
              "gender": "none",
              "hairColor": "none",
              "height": 200,
              "mass": 140,
              "skinColor": "metal"
            },
            {
              "name": "Arvel Crynyd",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": null,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Qui-Gon Jinn",
              "birthYear": "92BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 193,
              "mass": 89,
              "skinColor": "fair"
            },
            {
              "name": "R4-P17",
              "birthYear": "unknown",
              "eyeColor": "red, blue",
              "gender": "female",
              "hairColor": "none",
              "height": 96,
              "mass": null,
              "skinColor": "silver, red"
            }
          ]
        }
      }
    },
    {
      "name": "Wicket Systri Warrick",
      "birthYear": "8BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "brown",
      "height": 88,
      "mass": 20,
      "skinColor": "brown",
      "homeworld": {
        "name": "Endor",
        "diameter": 4900,
        "rotationPeriod": 18,
        "orbitalPeriod": 402,
        "gravity": "0.85 standard",
        "population": 30000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "forests",
          "mountains",
          "lakes"
        ],
        "surfaceWater": 8,
        "residentConnection": {
          "residents": [
            {
              "name": "Wicket Systri Warrick",
              "birthYear": "8BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 88,
              "mass": 20,
              "skinColor": "brown"
            }
          ]
        }
      }
    },
    {
      "name": "Nien Nunb",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 160,
      "mass": 68,
      "skinColor": "grey",
      "homeworld": {
        "name": "Sullust",
        "diameter": 12780,
        "rotationPeriod": 20,
        "orbitalPeriod": 263,
        "gravity": "1",
        "population": 18500000000,
        "climates": [
          "superheated"
        ],
        "terrains": [
          "mountains",
          "volcanoes",
          "rocky deserts"
        ],
        "surfaceWater": 5,
        "residentConnection": {
          "residents": [
            {
              "name": "Nien Nunb",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 160,
              "mass": 68,
              "skinColor": "grey"
            }
          ]
        }
      }
    },
    {
      "name": "Qui-Gon Jinn",
      "birthYear": "92BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 193,
      "mass": 89,
      "skinColor": "fair",
      "homeworld": {
        "name": "unknown",
        "diameter": 0,
        "rotationPeriod": 0,
        "orbitalPeriod": 0,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yoda",
              "birthYear": "896BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 66,
              "mass": 17,
              "skinColor": "green"
            },
            {
              "name": "IG-88",
              "birthYear": "15BBY",
              "eyeColor": "red",
              "gender": "none",
              "hairColor": "none",
              "height": 200,
              "mass": 140,
              "skinColor": "metal"
            },
            {
              "name": "Arvel Crynyd",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": null,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Qui-Gon Jinn",
              "birthYear": "92BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 193,
              "mass": 89,
              "skinColor": "fair"
            },
            {
              "name": "R4-P17",
              "birthYear": "unknown",
              "eyeColor": "red, blue",
              "gender": "female",
              "hairColor": "none",
              "height": 96,
              "mass": null,
              "skinColor": "silver, red"
            }
          ]
        }
      }
    },
    {
      "name": "Nute Gunray",
      "birthYear": "unknown",
      "eyeColor": "red",
      "gender": "male",
      "hairColor": "none",
      "height": 191,
      "mass": 90,
      "skinColor": "mottled green",
      "homeworld": {
        "name": "Cato Neimoidia",
        "diameter": 0,
        "rotationPeriod": 25,
        "orbitalPeriod": 278,
        "gravity": "1 standard",
        "population": 10000000,
        "climates": [
          "temperate",
          "moist"
        ],
        "terrains": [
          "mountains",
          "fields",
          "forests",
          "rock arches"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Nute Gunray",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "male",
              "hairColor": "none",
              "height": 191,
              "mass": 90,
              "skinColor": "mottled green"
            }
          ]
        }
      }
    },
    {
      "name": "Finis Valorum",
      "birthYear": "91BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "blond",
      "height": 170,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Coruscant",
        "diameter": 12240,
        "rotationPeriod": 24,
        "orbitalPeriod": 368,
        "gravity": "1 standard",
        "population": 1000000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "cityscape",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Finis Valorum",
              "birthYear": "91BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 170,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Adi Gallia",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "none",
              "height": 184,
              "mass": 50,
              "skinColor": "dark"
            },
            {
              "name": "Jocasta Nu",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "white",
              "height": 167,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Padmé Amidala",
      "birthYear": "46BBY",
      "eyeColor": "brown",
      "gender": "female",
      "hairColor": "brown",
      "height": 185,
      "mass": 45,
      "skinColor": "light",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Jar Jar Binks",
      "birthYear": "52BBY",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 196,
      "mass": 66,
      "skinColor": "orange",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Roos Tarpals",
      "birthYear": "unknown",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 224,
      "mass": 82,
      "skinColor": "grey",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Rugor Nass",
      "birthYear": "unknown",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 206,
      "mass": null,
      "skinColor": "green",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Ric Olié",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 183,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Watto",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "black",
      "height": 137,
      "mass": null,
      "skinColor": "blue, grey",
      "homeworld": {
        "name": "Toydaria",
        "diameter": 7900,
        "rotationPeriod": 21,
        "orbitalPeriod": 184,
        "gravity": "1",
        "population": 11000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "swamps",
          "lakes"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Watto",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "black",
              "height": 137,
              "mass": null,
              "skinColor": "blue, grey"
            }
          ]
        }
      }
    },
    {
      "name": "Sebulba",
      "birthYear": "unknown",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 112,
      "mass": 40,
      "skinColor": "grey, red",
      "homeworld": {
        "name": "Malastare",
        "diameter": 18880,
        "rotationPeriod": 26,
        "orbitalPeriod": 201,
        "gravity": "1.56",
        "population": 2000000000,
        "climates": [
          "arid",
          "temperate",
          "tropical"
        ],
        "terrains": [
          "swamps",
          "deserts",
          "jungles",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Sebulba",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 112,
              "mass": 40,
              "skinColor": "grey, red"
            }
          ]
        }
      }
    },
    {
      "name": "Quarsh Panaka",
      "birthYear": "62BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 183,
      "mass": null,
      "skinColor": "dark",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Shmi Skywalker",
      "birthYear": "72BBY",
      "eyeColor": "brown",
      "gender": "female",
      "hairColor": "black",
      "height": 163,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Darth Maul",
      "birthYear": "54BBY",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 175,
      "mass": 80,
      "skinColor": "red",
      "homeworld": {
        "name": "Dathomir",
        "diameter": 10480,
        "rotationPeriod": 24,
        "orbitalPeriod": 491,
        "gravity": "0.9",
        "population": 5200,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "forests",
          "deserts",
          "savannas"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Darth Maul",
              "birthYear": "54BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 175,
              "mass": 80,
              "skinColor": "red"
            }
          ]
        }
      }
    },
    {
      "name": "Bib Fortuna",
      "birthYear": "unknown",
      "eyeColor": "pink",
      "gender": "male",
      "hairColor": "none",
      "height": 180,
      "mass": null,
      "skinColor": "pale",
      "homeworld": {
        "name": "Ryloth",
        "diameter": 10600,
        "rotationPeriod": 30,
        "orbitalPeriod": 305,
        "gravity": "1",
        "population": 1500000000,
        "climates": [
          "temperate",
          "arid",
          "subartic"
        ],
        "terrains": [
          "mountains",
          "valleys",
          "deserts",
          "tundra"
        ],
        "surfaceWater": 5,
        "residentConnection": {
          "residents": [
            {
              "name": "Bib Fortuna",
              "birthYear": "unknown",
              "eyeColor": "pink",
              "gender": "male",
              "hairColor": "none",
              "height": 180,
              "mass": null,
              "skinColor": "pale"
            },
            {
              "name": "Ayla Secura",
              "birthYear": "48BBY",
              "eyeColor": "hazel",
              "gender": "female",
              "hairColor": "none",
              "height": 178,
              "mass": 55,
              "skinColor": "blue"
            }
          ]
        }
      }
    },
    {
      "name": "Ayla Secura",
      "birthYear": "48BBY",
      "eyeColor": "hazel",
      "gender": "female",
      "hairColor": "none",
      "height": 178,
      "mass": 55,
      "skinColor": "blue",
      "homeworld": {
        "name": "Ryloth",
        "diameter": 10600,
        "rotationPeriod": 30,
        "orbitalPeriod": 305,
        "gravity": "1",
        "population": 1500000000,
        "climates": [
          "temperate",
          "arid",
          "subartic"
        ],
        "terrains": [
          "mountains",
          "valleys",
          "deserts",
          "tundra"
        ],
        "surfaceWater": 5,
        "residentConnection": {
          "residents": [
            {
              "name": "Bib Fortuna",
              "birthYear": "unknown",
              "eyeColor": "pink",
              "gender": "male",
              "hairColor": "none",
              "height": 180,
              "mass": null,
              "skinColor": "pale"
            },
            {
              "name": "Ayla Secura",
              "birthYear": "48BBY",
              "eyeColor": "hazel",
              "gender": "female",
              "hairColor": "none",
              "height": 178,
              "mass": 55,
              "skinColor": "blue"
            }
          ]
        }
      }
    },
    {
      "name": "Ratts Tyerel",
      "birthYear": "unknown",
      "eyeColor": "unknown",
      "gender": "male",
      "hairColor": "none",
      "height": 79,
      "mass": 15,
      "skinColor": "grey, blue",
      "homeworld": {
        "name": "Aleen Minor",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Ratts Tyerel",
              "birthYear": "unknown",
              "eyeColor": "unknown",
              "gender": "male",
              "hairColor": "none",
              "height": 79,
              "mass": 15,
              "skinColor": "grey, blue"
            }
          ]
        }
      }
    },
    {
      "name": "Dud Bolt",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 94,
      "mass": 45,
      "skinColor": "blue, grey",
      "homeworld": {
        "name": "Vulpter",
        "diameter": 14900,
        "rotationPeriod": 22,
        "orbitalPeriod": 391,
        "gravity": "1",
        "population": 421000000,
        "climates": [
          "temperate",
          "artic"
        ],
        "terrains": [
          "urban",
          "barren"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Dud Bolt",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 94,
              "mass": 45,
              "skinColor": "blue, grey"
            }
          ]
        }
      }
    },
    {
      "name": "Gasgano",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 122,
      "mass": null,
      "skinColor": "white, blue",
      "homeworld": {
        "name": "Troiken",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "desert",
          "tundra",
          "rainforests",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Gasgano",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 122,
              "mass": null,
              "skinColor": "white, blue"
            }
          ]
        }
      }
    },
    {
      "name": "Ben Quadinaros",
      "birthYear": "unknown",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 163,
      "mass": 65,
      "skinColor": "grey, green, yellow",
      "homeworld": {
        "name": "Tund",
        "diameter": 12190,
        "rotationPeriod": 48,
        "orbitalPeriod": 1770,
        "gravity": "unknown",
        "population": 0,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "barren",
          "ash"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Ben Quadinaros",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 163,
              "mass": 65,
              "skinColor": "grey, green, yellow"
            }
          ]
        }
      }
    },
    {
      "name": "Mace Windu",
      "birthYear": "72BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "none",
      "height": 188,
      "mass": 84,
      "skinColor": "dark",
      "homeworld": {
        "name": "Haruun Kal",
        "diameter": 10120,
        "rotationPeriod": 25,
        "orbitalPeriod": 383,
        "gravity": "0.98",
        "population": 705300,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "toxic cloudsea",
          "plateaus",
          "volcanoes"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Mace Windu",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "none",
              "height": 188,
              "mass": 84,
              "skinColor": "dark"
            }
          ]
        }
      }
    },
    {
      "name": "Ki-Adi-Mundi",
      "birthYear": "92BBY",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "white",
      "height": 198,
      "mass": 82,
      "skinColor": "pale",
      "homeworld": {
        "name": "Cerea",
        "diameter": null,
        "rotationPeriod": 27,
        "orbitalPeriod": 386,
        "gravity": "1",
        "population": 450000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "verdant"
        ],
        "surfaceWater": 20,
        "residentConnection": {
          "residents": [
            {
              "name": "Ki-Adi-Mundi",
              "birthYear": "92BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "white",
              "height": 198,
              "mass": 82,
              "skinColor": "pale"
            }
          ]
        }
      }
    },
    {
      "name": "Kit Fisto",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 196,
      "mass": 87,
      "skinColor": "green",
      "homeworld": {
        "name": "Glee Anselm",
        "diameter": 15600,
        "rotationPeriod": 33,
        "orbitalPeriod": 206,
        "gravity": "1",
        "population": 500000000,
        "climates": [
          "tropical",
          "temperate"
        ],
        "terrains": [
          "lakes",
          "islands",
          "swamps",
          "seas"
        ],
        "surfaceWater": 80,
        "residentConnection": {
          "residents": [
            {
              "name": "Kit Fisto",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 87,
              "skinColor": "green"
            }
          ]
        }
      }
    },
    {
      "name": "Eeth Koth",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 171,
      "mass": null,
      "skinColor": "brown",
      "homeworld": {
        "name": "Iridonia",
        "diameter": null,
        "rotationPeriod": 29,
        "orbitalPeriod": 413,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "rocky canyons",
          "acid pools"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Eeth Koth",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 171,
              "mass": null,
              "skinColor": "brown"
            }
          ]
        }
      }
    },
    {
      "name": "Adi Gallia",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "none",
      "height": 184,
      "mass": 50,
      "skinColor": "dark",
      "homeworld": {
        "name": "Coruscant",
        "diameter": 12240,
        "rotationPeriod": 24,
        "orbitalPeriod": 368,
        "gravity": "1 standard",
        "population": 1000000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "cityscape",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Finis Valorum",
              "birthYear": "91BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 170,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Adi Gallia",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "none",
              "height": 184,
              "mass": 50,
              "skinColor": "dark"
            },
            {
              "name": "Jocasta Nu",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "white",
              "height": 167,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Saesee Tiin",
      "birthYear": "unknown",
      "eyeColor": "orange",
      "gender": "male",
      "hairColor": "none",
      "height": 188,
      "mass": null,
      "skinColor": "pale",
      "homeworld": {
        "name": "Iktotch",
        "diameter": null,
        "rotationPeriod": 22,
        "orbitalPeriod": 481,
        "gravity": "1",
        "population": null,
        "climates": [
          "arid",
          "rocky",
          "windy"
        ],
        "terrains": [
          "rocky"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Saesee Tiin",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 188,
              "mass": null,
              "skinColor": "pale"
            }
          ]
        }
      }
    },
    {
      "name": "Yarael Poof",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 264,
      "mass": null,
      "skinColor": "white",
      "homeworld": {
        "name": "Quermia",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yarael Poof",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 264,
              "mass": null,
              "skinColor": "white"
            }
          ]
        }
      }
    },
    {
      "name": "Plo Koon",
      "birthYear": "22BBY",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 188,
      "mass": 80,
      "skinColor": "orange",
      "homeworld": {
        "name": "Dorin",
        "diameter": 13400,
        "rotationPeriod": 22,
        "orbitalPeriod": 409,
        "gravity": "1",
        "population": null,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Plo Koon",
              "birthYear": "22BBY",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 188,
              "mass": 80,
              "skinColor": "orange"
            }
          ]
        }
      }
    },
    {
      "name": "Mas Amedda",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "none",
      "height": 196,
      "mass": null,
      "skinColor": "blue",
      "homeworld": {
        "name": "Champala",
        "diameter": null,
        "rotationPeriod": 27,
        "orbitalPeriod": 318,
        "gravity": "1",
        "population": 3500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "oceans",
          "rainforests",
          "plateaus"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Mas Amedda",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": null,
              "skinColor": "blue"
            }
          ]
        }
      }
    },
    {
      "name": "Gregar Typho",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 185,
      "mass": 85,
      "skinColor": "dark",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Cordé",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "female",
      "hairColor": "brown",
      "height": 157,
      "mass": null,
      "skinColor": "light",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Cliegg Lars",
      "birthYear": "82BBY",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 183,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Tatooine",
        "diameter": 10465,
        "rotationPeriod": 23,
        "orbitalPeriod": 304,
        "gravity": "1 standard",
        "population": 200000,
        "climates": [
          "arid"
        ],
        "terrains": [
          "desert"
        ],
        "surfaceWater": 1,
        "residentConnection": {
          "residents": [
            {
              "name": "Luke Skywalker",
              "birthYear": "19BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 172,
              "mass": 77,
              "skinColor": "fair"
            },
            {
              "name": "C-3PO",
              "birthYear": "112BBY",
              "eyeColor": "yellow",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 167,
              "mass": 75,
              "skinColor": "gold"
            },
            {
              "name": "Darth Vader",
              "birthYear": "41.9BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 202,
              "mass": 136,
              "skinColor": "white"
            },
            {
              "name": "Owen Lars",
              "birthYear": "52BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown, grey",
              "height": 178,
              "mass": 120,
              "skinColor": "light"
            },
            {
              "name": "Beru Whitesun lars",
              "birthYear": "47BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": 75,
              "skinColor": "light"
            },
            {
              "name": "R5-D4",
              "birthYear": "unknown",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 97,
              "mass": 32,
              "skinColor": "white, red"
            },
            {
              "name": "Biggs Darklighter",
              "birthYear": "24BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 84,
              "skinColor": "light"
            },
            {
              "name": "Anakin Skywalker",
              "birthYear": "41.9BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 188,
              "mass": 84,
              "skinColor": "fair"
            },
            {
              "name": "Shmi Skywalker",
              "birthYear": "72BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "black",
              "height": 163,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Cliegg Lars",
              "birthYear": "82BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Poggle the Lesser",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 183,
      "mass": 80,
      "skinColor": "green",
      "homeworld": {
        "name": "Geonosis",
        "diameter": 11370,
        "rotationPeriod": 30,
        "orbitalPeriod": 256,
        "gravity": "0.9 standard",
        "population": 100000000000,
        "climates": [
          "temperate",
          "arid"
        ],
        "terrains": [
          "rock",
          "desert",
          "mountain",
          "barren"
        ],
        "surfaceWater": 5,
        "residentConnection": {
          "residents": [
            {
              "name": "Poggle the Lesser",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 183,
              "mass": 80,
              "skinColor": "green"
            }
          ]
        }
      }
    },
    {
      "name": "Luminara Unduli",
      "birthYear": "58BBY",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "black",
      "height": 170,
      "mass": 56.2,
      "skinColor": "yellow",
      "homeworld": {
        "name": "Mirial",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "deserts"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Luminara Unduli",
              "birthYear": "58BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "black",
              "height": 170,
              "mass": 56.2,
              "skinColor": "yellow"
            },
            {
              "name": "Barriss Offee",
              "birthYear": "40BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "black",
              "height": 166,
              "mass": 50,
              "skinColor": "yellow"
            }
          ]
        }
      }
    },
    {
      "name": "Barriss Offee",
      "birthYear": "40BBY",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "black",
      "height": 166,
      "mass": 50,
      "skinColor": "yellow",
      "homeworld": {
        "name": "Mirial",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "deserts"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Luminara Unduli",
              "birthYear": "58BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "black",
              "height": 170,
              "mass": 56.2,
              "skinColor": "yellow"
            },
            {
              "name": "Barriss Offee",
              "birthYear": "40BBY",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "black",
              "height": 166,
              "mass": 50,
              "skinColor": "yellow"
            }
          ]
        }
      }
    },
    {
      "name": "Dormé",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "female",
      "hairColor": "brown",
      "height": 165,
      "mass": null,
      "skinColor": "light",
      "homeworld": {
        "name": "Naboo",
        "diameter": 12120,
        "rotationPeriod": 26,
        "orbitalPeriod": 312,
        "gravity": "1 standard",
        "population": 4500000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grassy hills",
          "swamps",
          "forests",
          "mountains"
        ],
        "surfaceWater": 12,
        "residentConnection": {
          "residents": [
            {
              "name": "R2-D2",
              "birthYear": "33BBY",
              "eyeColor": "red",
              "gender": "n/a",
              "hairColor": "n/a",
              "height": 96,
              "mass": 32,
              "skinColor": "white, blue"
            },
            {
              "name": "Palpatine",
              "birthYear": "82BBY",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "grey",
              "height": 170,
              "mass": 75,
              "skinColor": "pale"
            },
            {
              "name": "Padmé Amidala",
              "birthYear": "46BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 185,
              "mass": 45,
              "skinColor": "light"
            },
            {
              "name": "Jar Jar Binks",
              "birthYear": "52BBY",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 196,
              "mass": 66,
              "skinColor": "orange"
            },
            {
              "name": "Roos Tarpals",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 224,
              "mass": 82,
              "skinColor": "grey"
            },
            {
              "name": "Rugor Nass",
              "birthYear": "unknown",
              "eyeColor": "orange",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": null,
              "skinColor": "green"
            },
            {
              "name": "Ric Olié",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 183,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Quarsh Panaka",
              "birthYear": "62BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": null,
              "skinColor": "dark"
            },
            {
              "name": "Gregar Typho",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 185,
              "mass": 85,
              "skinColor": "dark"
            },
            {
              "name": "Cordé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 157,
              "mass": null,
              "skinColor": "light"
            },
            {
              "name": "Dormé",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 165,
              "mass": null,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Dooku",
      "birthYear": "102BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "white",
      "height": 193,
      "mass": 80,
      "skinColor": "fair",
      "homeworld": {
        "name": "Serenno",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "rainforests",
          "rivers",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Dooku",
              "birthYear": "102BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 193,
              "mass": 80,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "Bail Prestor Organa",
      "birthYear": "67BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 191,
      "mass": null,
      "skinColor": "tan",
      "homeworld": {
        "name": "Alderaan",
        "diameter": 12500,
        "rotationPeriod": 24,
        "orbitalPeriod": 364,
        "gravity": "1 standard",
        "population": 2000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grasslands",
          "mountains"
        ],
        "surfaceWater": 40,
        "residentConnection": {
          "residents": [
            {
              "name": "Leia Organa",
              "birthYear": "19BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 150,
              "mass": 49,
              "skinColor": "light"
            },
            {
              "name": "Bail Prestor Organa",
              "birthYear": "67BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 191,
              "mass": null,
              "skinColor": "tan"
            },
            {
              "name": "Raymus Antilles",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 188,
              "mass": 79,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Jango Fett",
      "birthYear": "66BBY",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "black",
      "height": 183,
      "mass": 79,
      "skinColor": "tan",
      "homeworld": {
        "name": "Concord Dawn",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "jungles",
          "forests",
          "deserts"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Jango Fett",
              "birthYear": "66BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 79,
              "skinColor": "tan"
            }
          ]
        }
      }
    },
    {
      "name": "Zam Wesell",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "female",
      "hairColor": "blonde",
      "height": 168,
      "mass": 55,
      "skinColor": "fair, green, yellow",
      "homeworld": {
        "name": "Zolan",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Zam Wesell",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "female",
              "hairColor": "blonde",
              "height": 168,
              "mass": 55,
              "skinColor": "fair, green, yellow"
            }
          ]
        }
      }
    },
    {
      "name": "Dexter Jettster",
      "birthYear": "unknown",
      "eyeColor": "yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 198,
      "mass": 102,
      "skinColor": "brown",
      "homeworld": {
        "name": "Ojom",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": 500000000,
        "climates": [
          "frigid"
        ],
        "terrains": [
          "oceans",
          "glaciers"
        ],
        "surfaceWater": 100,
        "residentConnection": {
          "residents": [
            {
              "name": "Dexter Jettster",
              "birthYear": "unknown",
              "eyeColor": "yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 198,
              "mass": 102,
              "skinColor": "brown"
            }
          ]
        }
      }
    },
    {
      "name": "Lama Su",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 229,
      "mass": 88,
      "skinColor": "grey",
      "homeworld": {
        "name": "Kamino",
        "diameter": 19720,
        "rotationPeriod": 27,
        "orbitalPeriod": 463,
        "gravity": "1 standard",
        "population": 1000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "ocean"
        ],
        "surfaceWater": 100,
        "residentConnection": {
          "residents": [
            {
              "name": "Boba Fett",
              "birthYear": "31.5BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 78.2,
              "skinColor": "fair"
            },
            {
              "name": "Lama Su",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 229,
              "mass": 88,
              "skinColor": "grey"
            },
            {
              "name": "Taun We",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "female",
              "hairColor": "none",
              "height": 213,
              "mass": null,
              "skinColor": "grey"
            }
          ]
        }
      }
    },
    {
      "name": "Taun We",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "female",
      "hairColor": "none",
      "height": 213,
      "mass": null,
      "skinColor": "grey",
      "homeworld": {
        "name": "Kamino",
        "diameter": 19720,
        "rotationPeriod": 27,
        "orbitalPeriod": 463,
        "gravity": "1 standard",
        "population": 1000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "ocean"
        ],
        "surfaceWater": 100,
        "residentConnection": {
          "residents": [
            {
              "name": "Boba Fett",
              "birthYear": "31.5BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 183,
              "mass": 78.2,
              "skinColor": "fair"
            },
            {
              "name": "Lama Su",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 229,
              "mass": 88,
              "skinColor": "grey"
            },
            {
              "name": "Taun We",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "female",
              "hairColor": "none",
              "height": 213,
              "mass": null,
              "skinColor": "grey"
            }
          ]
        }
      }
    },
    {
      "name": "Jocasta Nu",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "female",
      "hairColor": "white",
      "height": 167,
      "mass": null,
      "skinColor": "fair",
      "homeworld": {
        "name": "Coruscant",
        "diameter": 12240,
        "rotationPeriod": 24,
        "orbitalPeriod": 368,
        "gravity": "1 standard",
        "population": 1000000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "cityscape",
          "mountains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Finis Valorum",
              "birthYear": "91BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "blond",
              "height": 170,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Adi Gallia",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "none",
              "height": 184,
              "mass": 50,
              "skinColor": "dark"
            },
            {
              "name": "Jocasta Nu",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "female",
              "hairColor": "white",
              "height": 167,
              "mass": null,
              "skinColor": "fair"
            }
          ]
        }
      }
    },
    {
      "name": "R4-P17",
      "birthYear": "unknown",
      "eyeColor": "red, blue",
      "gender": "female",
      "hairColor": "none",
      "height": 96,
      "mass": null,
      "skinColor": "silver, red",
      "homeworld": {
        "name": "unknown",
        "diameter": 0,
        "rotationPeriod": 0,
        "orbitalPeriod": 0,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Yoda",
              "birthYear": "896BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "white",
              "height": 66,
              "mass": 17,
              "skinColor": "green"
            },
            {
              "name": "IG-88",
              "birthYear": "15BBY",
              "eyeColor": "red",
              "gender": "none",
              "hairColor": "none",
              "height": 200,
              "mass": 140,
              "skinColor": "metal"
            },
            {
              "name": "Arvel Crynyd",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": null,
              "mass": null,
              "skinColor": "fair"
            },
            {
              "name": "Qui-Gon Jinn",
              "birthYear": "92BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 193,
              "mass": 89,
              "skinColor": "fair"
            },
            {
              "name": "R4-P17",
              "birthYear": "unknown",
              "eyeColor": "red, blue",
              "gender": "female",
              "hairColor": "none",
              "height": 96,
              "mass": null,
              "skinColor": "silver, red"
            }
          ]
        }
      }
    },
    {
      "name": "Wat Tambor",
      "birthYear": "unknown",
      "eyeColor": "unknown",
      "gender": "male",
      "hairColor": "none",
      "height": 193,
      "mass": 48,
      "skinColor": "green, grey",
      "homeworld": {
        "name": "Skako",
        "diameter": null,
        "rotationPeriod": 27,
        "orbitalPeriod": 384,
        "gravity": "1",
        "population": 500000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "urban",
          "vines"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Wat Tambor",
              "birthYear": "unknown",
              "eyeColor": "unknown",
              "gender": "male",
              "hairColor": "none",
              "height": 193,
              "mass": 48,
              "skinColor": "green, grey"
            }
          ]
        }
      }
    },
    {
      "name": "San Hill",
      "birthYear": "unknown",
      "eyeColor": "gold",
      "gender": "male",
      "hairColor": "none",
      "height": 191,
      "mass": null,
      "skinColor": "grey",
      "homeworld": {
        "name": "Muunilinst",
        "diameter": 13800,
        "rotationPeriod": 28,
        "orbitalPeriod": 412,
        "gravity": "1",
        "population": 5000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "plains",
          "forests",
          "hills",
          "mountains"
        ],
        "surfaceWater": 25,
        "residentConnection": {
          "residents": [
            {
              "name": "San Hill",
              "birthYear": "unknown",
              "eyeColor": "gold",
              "gender": "male",
              "hairColor": "none",
              "height": 191,
              "mass": null,
              "skinColor": "grey"
            }
          ]
        }
      }
    },
    {
      "name": "Shaak Ti",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "female",
      "hairColor": "none",
      "height": 178,
      "mass": 57,
      "skinColor": "red, blue, white",
      "homeworld": {
        "name": "Shili",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "1",
        "population": null,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "cities",
          "savannahs",
          "seas",
          "plains"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Shaak Ti",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "female",
              "hairColor": "none",
              "height": 178,
              "mass": 57,
              "skinColor": "red, blue, white"
            }
          ]
        }
      }
    },
    {
      "name": "Grievous",
      "birthYear": "unknown",
      "eyeColor": "green, yellow",
      "gender": "male",
      "hairColor": "none",
      "height": 216,
      "mass": 159,
      "skinColor": "brown, white",
      "homeworld": {
        "name": "Kalee",
        "diameter": 13850,
        "rotationPeriod": 23,
        "orbitalPeriod": 378,
        "gravity": "1",
        "population": 4000000000,
        "climates": [
          "arid",
          "temperate",
          "tropical"
        ],
        "terrains": [
          "rainforests",
          "cliffs",
          "canyons",
          "seas"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Grievous",
              "birthYear": "unknown",
              "eyeColor": "green, yellow",
              "gender": "male",
              "hairColor": "none",
              "height": 216,
              "mass": 159,
              "skinColor": "brown, white"
            }
          ]
        }
      }
    },
    {
      "name": "Tarfful",
      "birthYear": "unknown",
      "eyeColor": "blue",
      "gender": "male",
      "hairColor": "brown",
      "height": 234,
      "mass": 136,
      "skinColor": "brown",
      "homeworld": {
        "name": "Kashyyyk",
        "diameter": 12765,
        "rotationPeriod": 26,
        "orbitalPeriod": 381,
        "gravity": "1 standard",
        "population": 45000000,
        "climates": [
          "tropical"
        ],
        "terrains": [
          "jungle",
          "forests",
          "lakes",
          "rivers"
        ],
        "surfaceWater": 60,
        "residentConnection": {
          "residents": [
            {
              "name": "Chewbacca",
              "birthYear": "200BBY",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 228,
              "mass": 112,
              "skinColor": "unknown"
            },
            {
              "name": "Tarfful",
              "birthYear": "unknown",
              "eyeColor": "blue",
              "gender": "male",
              "hairColor": "brown",
              "height": 234,
              "mass": 136,
              "skinColor": "brown"
            }
          ]
        }
      }
    },
    {
      "name": "Raymus Antilles",
      "birthYear": "unknown",
      "eyeColor": "brown",
      "gender": "male",
      "hairColor": "brown",
      "height": 188,
      "mass": 79,
      "skinColor": "light",
      "homeworld": {
        "name": "Alderaan",
        "diameter": 12500,
        "rotationPeriod": 24,
        "orbitalPeriod": 364,
        "gravity": "1 standard",
        "population": 2000000000,
        "climates": [
          "temperate"
        ],
        "terrains": [
          "grasslands",
          "mountains"
        ],
        "surfaceWater": 40,
        "residentConnection": {
          "residents": [
            {
              "name": "Leia Organa",
              "birthYear": "19BBY",
              "eyeColor": "brown",
              "gender": "female",
              "hairColor": "brown",
              "height": 150,
              "mass": 49,
              "skinColor": "light"
            },
            {
              "name": "Bail Prestor Organa",
              "birthYear": "67BBY",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "black",
              "height": 191,
              "mass": null,
              "skinColor": "tan"
            },
            {
              "name": "Raymus Antilles",
              "birthYear": "unknown",
              "eyeColor": "brown",
              "gender": "male",
              "hairColor": "brown",
              "height": 188,
              "mass": 79,
              "skinColor": "light"
            }
          ]
        }
      }
    },
    {
      "name": "Sly Moore",
      "birthYear": "unknown",
      "eyeColor": "white",
      "gender": "female",
      "hairColor": "none",
      "height": 178,
      "mass": 48,
      "skinColor": "pale",
      "homeworld": {
        "name": "Umbara",
        "diameter": null,
        "rotationPeriod": null,
        "orbitalPeriod": null,
        "gravity": "unknown",
        "population": null,
        "climates": [
          "unknown"
        ],
        "terrains": [
          "unknown"
        ],
        "surfaceWater": null,
        "residentConnection": {
          "residents": [
            {
              "name": "Sly Moore",
              "birthYear": "unknown",
              "eyeColor": "white",
              "gender": "female",
              "hairColor": "none",
              "height": 178,
              "mass": 48,
              "skinColor": "pale"
            }
          ]
        }
      }
    },
    {
      "name": "Tion Medon",
      "birthYear": "unknown",
      "eyeColor": "black",
      "gender": "male",
      "hairColor": "none",
      "height": 206,
      "mass": 80,
      "skinColor": "grey",
      "homeworld": {
        "name": "Utapau",
        "diameter": 12900,
        "rotationPeriod": 27,
        "orbitalPeriod": 351,
        "gravity": "1 standard",
        "population": 95000000,
        "climates": [
          "temperate",
          "arid",
          "windy"
        ],
        "terrains": [
          "scrublands",
          "savanna",
          "canyons",
          "sinkholes"
        ],
        "surfaceWater": 0.9,
        "residentConnection": {
          "residents": [
            {
              "name": "Tion Medon",
              "birthYear": "unknown",
              "eyeColor": "black",
              "gender": "male",
              "hairColor": "none",
              "height": 206,
              "mass": 80,
              "skinColor": "grey"
            }
          ]
        }
      }
    }
  ]


module.exports = data;