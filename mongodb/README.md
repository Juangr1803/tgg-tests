**Torneos.GG Test: MongoDB**

This test has been made to validate the knowledge of a job applicant. The test duration should not be above 30 minutes.

**What you need:**

1. Internet connection
2. A code editor
3. Git
4. Share your screen

**The goal:**

1. Establish a connection to a MongoDB cluster.
2. Manage collections and documents
3. Create CRUD operations
4. Use Array functions
5. Insert all the dummy data into a collection (data/dummy.js)

CREDENTIALS:
mongodb+srv://test_user:test123@cluster1.29pto.mongodb.net/TEST?retryWrites=true&w=majority
